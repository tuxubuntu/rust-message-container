extern crate containerizer;

type Result<T> = std::result::Result<T, &'static str>;

mod message;

pub use message::{ Message, CODE_MESSAGE };

#[cfg(test)]
mod tests;
