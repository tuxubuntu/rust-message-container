
# Basic protocol

### Requests structure

```

[ u8 ][ u20 ][ u4 ][ []u8 ][ []u8 ]
  |      |     |       |       |
  |      |     |       |       \ (E) Message body
  |      |     |       |
  |      |     |       \ (D) Body length (in bytes)
  |      |     |
  |      |     \ (C) Length of length (in bytes)
  |      |
  |      \ (B) Message ID
  |
  \ (A) Command
.

```

If Command is `0`, then it's response to message with similar MessageID.

If MessageID is `0`, then message don't need of answer.

MessageID allows use up to `1048575` concurrent messages.

#### Allows

```
A = [0..255]

B = [0..1048575]

C = [0..15]

---

length of D = u8 * C // [0..120] bit

length of D = u8 * C // [0..960] bit

length of E = u8 * 2 ^ D // [0..(2^(8*15))] bytes

max length of message = 1329227995784915872903807060280344576 bytes
```

### Overhead per message

```
8+20+4 = 32 // empty message
32+8*15 = 152 // under 1_329_227_995_784 yottabytes
```
