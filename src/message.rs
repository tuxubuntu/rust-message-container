
use containerizer::Container;

pub struct Message;

pub const CODE_MESSAGE: u8 = 46;
const ERR_WRONG_CODE: &str = "Wrong code";

use super::Result;

type Args = (u8, u32, Vec<u8>);

impl Container<Args, Vec<u8>> for Message {
	fn code() -> u8 {
		CODE_MESSAGE
	}
	fn encode(data: Args) -> Result<Vec<u8>> {
		let (code, mid, body) = data;
		let mut res = Vec::new();
		res.push(Message::code());
		res.push(code);
		{
			use std::mem::transmute;
			let bytes: [u8; 4] = unsafe { transmute((mid).to_le()) };
			res.extend(bytes.to_vec());
		};
		res.extend(body);
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Args> {
		if data[0] != Message::code() {
			return Err(ERR_WRONG_CODE)
		}
		let code = data[1];
		let mid = {
			use std::mem::transmute;
			let mut a: [u8; 4] = Default::default();
			a.copy_from_slice(&data[2..6]);
			let mid: u32 = unsafe { transmute(a) };
			mid
		};
		let body = data[6..].to_vec();
		Ok((code, mid, body))
	}
}
