
#[test]
fn message() {
	use super::Message;
	use containerizer::Container;

	let res = Message::encode((2u8, 32, vec![1,2,3])).unwrap();
	//                         |    |         |
	//                         |    |         \ Body
	//                         |    |
	//                         |    \ MessageID
	//                         |
	//                         \ Command
	assert_eq!(Message::decode(res).unwrap(), (2u8, 32, vec![1,2,3]) );
}
